
<section>
    <div class="row">
    <?php foreach ($productos as $p){?>

        <div class="col-4">
        
        <div class="card mb-4 box-shadow">
                <img class="card-img-top" alt="Thumbnail" style="height: 200px; width: 90%; display: block;" src="<?=base_url().$p->img_prod?>" data-holder-rendered="true">
                <div class="card-body">
                    <h4><?= $p->titulo_prod?></h4>
                  <p style="height: 150px; width:250px; display: block;" class="card-text"><?= $p->desc_prod?></p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <a href="<?=base_url()?>Detail/product/<?=$p->id_prod?>" type="button" class="btn btn-sm btn-outline-secondary">Ver mas...</a>
                    </div>
                    <small class="text-muted">Cantidad :  <?= $p->stock_prod?></small>
                  </div>
                </div>
              </div>
        </div>
        <?php }?>
    </div>
</section>