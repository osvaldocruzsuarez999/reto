<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
  
    <link rel="stylesheet" href="<?=base_url()?>static/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>static/css/login.css">
    <script src="<?=base_url()?>static/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
  <script>
    base_url = "<?=base_url()?>"
  </script>
  <title>Document</title>
</head>
<body>
    <main class ="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <a href = "<?=base_url()?>" style ="text-decoration:none; color:#000;" class="my-0 mr-md-auto font-weight-normal">Tienda</a>  
  </main>
  <section class="login-content">
  <div class="card" style="width: 28rem;">
  <div class="card-body">
    <h5 class="card-title">Registro</h5>
    <form>
      
  <div class="mb-3">
    <label for="Inputname" class="form-label">Nombre</label>
    <input type="text" class="form-control" id="Inputname">
  </div>
  <div class="mb-3">
    <label for="InputEmail1" class="form-label">Correo electronico</label>
    <input type="email" class="form-control" id="Inputemail" aria-describedby="emailHelp">
  </div>
  <div class="mb-3">
    <label for="InputPassword1" class="form-label">Password</label>
    <input type="password" class="form-control" id="InputPassword">
  </div>
  
</form>    
<button onClick="registar()" class="btn btn-primary">Crear</button>
</div>
</div>
  </section>
    </body>
    <script src="<?=base_url()?>static/js/login.js"></script>
</html>