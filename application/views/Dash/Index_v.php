<div class="container-fluid">
        
        <main role="main" class=" pt-3 px-4"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
          
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Productos</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group mr-2">
                    <a href="<?=base_url()?>Dashboard/ListProducts/addProduct" class="btn btn-sm btn-outline-secondary">Agregar</a>
                </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Descripcion</th>
                  <th>Imagen</th>
                  <th>Stock</th>
                  <th>Acciones</th>
                  
                </tr>
              </thead>
              <tbody>
              <?php foreach ($prod as $p) {
               ?>
                <tr>
                  <td><?= $p->id_prod?></td>
                  <td><?= $p->titulo_prod?></td>
                  <td style = "width :250px; align-text:justify;"><?= $p->desc_prod?></td>
                  <td><img style ="display: block;margin: 0 auto;max-width: 100%;width: 80%;" src="<?=base_url().$p->img_prod?>" alt="<?= $p->id_prod?>"></td>
                  <td><?= $p->stock_prod?></td>
                  <td>
                  <div class="row">
                    <button class="btn btn-sm btn-outline-danger" onclick="eliminar('<?=$p->id_prod?>')">Eliminar</button>
                    <a href = "<?=base_url()."Dashboard/ListProducts/edit/".$p->id_prod?>" class="btn btn-sm btn-outline-secondary" >Editar</a>
                  
                  </div>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </main>
      
    </div>
    <script>
      function eliminar(id){
      Swal.fire({
  title: '¿Quieres eliminar?',
  showDenyButton: true,
  showCancelButton: false,
  confirmButtonText: 'Si',
  denyButtonText: `Cancelar`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
    $.ajax({
    type: 'POST',
    url: `${base_url}Dashboard/ListProducts/delete`,
    data:{
      id : id
    },
    success : function(json){
        console.log(json)
      if(json.response_code==200){
        Swal.fire('Eliminado!', '', 'success')
                  location.reload();
      }else{
        Swal.fire('Eliminado!', '', 'success')
                  location.reload();
      
      }
      }
    })
    //Swal.fire('Saved!', '', 'success')

  } else if (result.isDenied) {
    Swal.fire('Cancelado', '', 'info')
  }
})

}
    </script>