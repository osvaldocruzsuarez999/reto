<section>
  <?php foreach($datos as $d){
  } ?>

  <div class="container ">
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-10 col-xl-10">
        <div class="card text-black">
          <i class="fab fa-apple fa-lg pt-3 pb-1 px-3"></i>
          <img src="<?=base_url().$d->img_prod?>"
            class="card-img-top" alt="Apple Computer" />
          <div class="card-body">
            <div class="text-center">
              <h5 class="card-title"><?= $d->titulo_prod?></h5>
              <p class="text-muted mb-4"><?= $d->desc_prod?></p>
            </div>
            <div class="d-flex justify-content-between total font-weight-bold mt-4">
              <span></span><span>Cantidad : <?= $d->stock_prod?></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>