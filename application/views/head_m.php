<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
  
    <link rel="stylesheet" href="<?=base_url()?>static/css/bootstrap.min.css">
    <script src="<?=base_url()?>static/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
  <script>
    base_url = "<?=base_url()?>"
  </script>
  <link rel="stylesheet" href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
  <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
  <title>Document</title>
</head>
<body>
    <main class ="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <a href = "<?=base_url()?>" style ="text-decoration:none; color:#000;" class="my-0 mr-md-auto font-weight-normal">Tienda</a>
        <nav class="my-2 my-md-0 mr-md-3">
        <?php
      if(isset($n) && isset($t)){ 
      ?>
      <div style="display:flex;">
      <h4 style="color:#000;margin-right:10px;"><?=$n." : ".$t?></h4>  
        <a class="btn btn-outline-primary" href="<?=base_url()?>Salir/cerrar">Cerrar sesion</a>
 
      </div>
      <?php
    }else{
      ?>
 <a class="btn btn-outline-primary" href="<?=base_url()?>account/create">Registro</a>  
        <a class="btn btn-outline-primary" href="<?=base_url()?>login">Iniciar sesión</a>
       
      <?php
    }
    ?>
  
        
      </nav>
    </main>

    
