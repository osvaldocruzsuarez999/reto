<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListProducts extends CI_Controller{
    function __construct() {
        parent::__construct();
            
    }
	
    public function index(){
        if($this->session->userdata('signin')){
         
            $data["n"] = $this->session->userdata("nombre_usuario");
            $data["t"] = $this->session->userdata("tipo_user");
         
           
            $data["scripts"][] = 'static/js/productsindex.js';

            $this->load->model("productos_m");
            $registros = $this->productos_m->lista_pro();
            $data["prod"] = $registros;

            $this->load->view("head_m",$data);
            $this->load->view('Dash/index_v',$data);
            $this->load->view("footer_m");     
            
            if($this->session->userdata('tipo_user') == "cliente"){
                redirect('welcome','refresh');    
            }
        }else{
            redirect('welcome','refresh');
        }
    }
    public function addProduct(){
        $data["scripts"][] = 'static/js/products.js';
        $data["sesion"] = $this->session->userdata();    
		
		$data["n"] = $this->session->userdata("nombre_usuario");
		$data["t"] = $this->session->userdata("tipo_user");
	 
        if($this->session->userdata('signin')){
            $this->load->view("head_m",$data);
            $this->load->view('Dash/addProd_v');
            $this->load->view("footer_m",$data);     
        }else{
            redirect('welcome','refresh');
        }
    }
    public function delete(){
        $id = $this->input->post("id");
        $this->load->model("productos_m");
        $validar = $this->productos_m->delete($id);
        
        if(!is_null($validar)){
            $validar = (array)$validar;
            $json["response_code"] =200;
            $json["response_message"] ="correcto";
        }else{
            $json["response_code"] =400;
            $json["response_message"] ="error";
        }
        echo json_encode($json);
    
    }
    public function edit($id){
        $data["sesion"] = $this->session->userdata();    
		
		$data["n"] = $this->session->userdata("nombre_usuario");
		$data["t"] = $this->session->userdata("tipo_user");
	 
        $data["id"] = $id;
        $this->load->model("productos_m");
        $data["datos"] = $this->productos_m->detail($id);
        
        $data["scripts"][] = 'static/js/products.js';

        if($this->session->userdata('signin')){
            $this->load->view("head_m",$data);
            $this->load->view('Dash/editProd_v',$data);
            $this->load->view("footer_m",$data);     
        }else{
            redirect('welcome','refresh');
        }
    }
    public function addProduct_service(){
        $file = $this->input->post("file");

        if(!empty($_FILES['file']['name'])){
            $newnamefile = md5($_FILES['file']['name']);
            $typeimg = exif_imagetype($_FILES['file']['tmp_name']);
            
            switch ($typeimg) {
                case 'IMAGETYPE_GIF':
                $newnamefile = $newnamefile.'.gif';
                break;

                case 'IMAGETYPE_JPEG':
                $newnamefile = $newnamefile.'.jpeg';
                break;

                case 'IMAGETYPE_PNG':
                $newnamefile = $newnamefile.'.png';
                break;

                default:
                $newnamefile = $newnamefile.'.jpg';
                break;
            }
            $file['name'] = $newnamefile;
            $imagename = "static/upload/".$file['name'];
            $file = $_FILES['file'];

            $config['upload_path'] = "static/upload/";
            $config['allowed_types'] = "gif|jpg|jpeg|png";
            $config['file_name'] = $newnamefile;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload('file')){

                $error = $this->upload->display_errors();
                $json['code'] = "400";
                $json["data"] = $error;

            }else{
                $this->load->library('image_lib');
                $image_data =   $this->upload->data();

                $configer =  array(
                    'image_library'   => 'gd2',
                    'source_image'    =>  $image_data['full_path'],
                    'maintain_ratio'  =>  TRUE,
                    'width'           =>  300,
                    'height'          =>  300,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();

                $dataarray["titulo_prod"] = $this->input->post("Nombre");
                $dataarray["desc_prod"] = $this->input->post("desc");
                $dataarray["stock_prod"] = $this->input->post("stock");
                $dataarray["img_prod"] = $imagename;

                $this->load->model("productos_m");
                $val = $this->productos_m->up($dataarray);
                if($val){
                    $json['code'] = "200";
                    $json["data"] = $val;

                }else{
                    $json['code'] = "400";
                    $json["data"] = $val;
                }
            }

        }else{
            $json['code'] = "400";
            $json["data"] = "no entro en el primer if";
        
        }
        //echo json_encode($json);
        redirect("Dashboard/ListProducts","refresh");
    }
    public function editProduct_service(){
     
        $file = $this->input->post("file");
        $id = $this->input->post("id");

        if(!empty($_FILES['file']['name'])){
            $newnamefile = md5($_FILES['file']['name']);
            $typeimg = exif_imagetype($_FILES['file']['tmp_name']);
            
            switch ($typeimg) {
                case 'IMAGETYPE_GIF':
                $newnamefile = $newnamefile.'.gif';
                break;

                case 'IMAGETYPE_JPEG':
                $newnamefile = $newnamefile.'.jpeg';
                break;

                case 'IMAGETYPE_PNG':
                $newnamefile = $newnamefile.'.png';
                break;

                default:
                $newnamefile = $newnamefile.'.jpg';
                break;
            }
            $file['name'] = $newnamefile;
            $imagename = "static/upload/".$file['name'];
            $file = $_FILES['file'];

            $config['upload_path'] = "static/upload/";
            $config['allowed_types'] = "gif|jpg|jpeg|png";
            $config['file_name'] = $newnamefile;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload('file')){

                $error = $this->upload->display_errors();
                $json['code'] = "400";
                $json["data"] = $error;

            }else{
                $this->load->library('image_lib');
                $image_data =   $this->upload->data();

                $configer =  array(
                    'image_library'   => 'gd2',
                    'source_image'    =>  $image_data['full_path'],
                    'maintain_ratio'  =>  TRUE,
                    'width'           =>  300,
                    'height'          =>  300,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();

                $dataarray["titulo_prod"] = $this->input->post("Nombre");
                $dataarray["desc_prod"] = $this->input->post("desc");
                $dataarray["stock_prod"] = $this->input->post("stock");
                $dataarray["img_prod"] = $imagename;

                $this->load->model("productos_m");
                $val = $this->productos_m->edit($id,$dataarray);
                if($val){
                    $json['code'] = "200";
                    $json["data"] = $val;

                }else{
                    $json['code'] = "400";
                    $json["data"] = $val;
                }
            }

        }else{
         
            $dataarray["titulo_prod"] = $this->input->post("Nombre");
                $dataarray["desc_prod"] = $this->input->post("desc");
                $dataarray["stock_prod"] = $this->input->post("stock");
                
                $this->load->model("productos_m");
                $val = $this->productos_m->edit($id,$dataarray);
                if($val){
                    $json['code'] = "200";
                    $json["data"] = $val;

                }else{
                    $json['code'] = "400";
                    $json["data"] = $val;
                }
        }
        //echo json_encode($json);
        redirect("Dashboard/ListProducts","refresh");
       
    }
}