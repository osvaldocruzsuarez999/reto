<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller{
    public function create(){
        $this->load->view("register_v");
    }
    public function create_service(){
        $name  = $this->input->post("name");
        $mail = $this->input->post("mail");
        $pass = $this->input->post("pass");
        $pass = md5($pass);

        $this->load->model("auth_model");
        $validar = $this->auth_model->register($name,$mail,$pass);
        if(!is_null($validar)){
            $validar = (array)$validar;
            $json["response_code"] =200;
            $json["response_message"] ="correcto";
        }else{
            $json["response_code"] =400;
            $json["response_message"] ="error";
        }
        echo json_encode($json);
    }
    public function auth_service(){
        $mail = $this->input->post("mail");
        $pass = $this->input->post("pass");
        $pass = md5($pass);

        $this->load->model("auth_model");
        $validar = $this->auth_model->login_validar($mail,$pass);
        
        if(!is_null($validar)){
            if($validar->pass_usuario===$pass){
                $validar = (array)$validar;
                $validar["signin"]  = TRUE;
                $json["response_code"] =200;
                $json["response_message"] ="correcto";
                $this->session->set_userdata($validar);
            }else{
              
            $json["response_code"] =400;
            $json["response_message"] ="credenciales incorrectas";
          
            }
        }else{
            $json["response_code"] =400;
            $json["response_message"] ="error";
        }
        echo json_encode($json);
    
    }
}