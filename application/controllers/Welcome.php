<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */

	function __construct() {
        parent::__construct();
        //load pagination library
        $this->load->library('pagination');
        //load post model
        $this->load->model('productos_m');
        //per page limit
        $this->perPage = 9;
		
    }
	public function index()
	{
		$data["sesion"] = $this->session->userdata();    
		
		$data["n"] = $this->session->userdata("nombre_usuario");
		$data["t"] = $this->session->userdata("tipo_user");
	 
		//echo $this->productos_m->count();
	
		$pag_size = 9;
		$offset = 0;		//echo $this->productos_m->pagination(9,0);
		$data["productos"] = $this->productos_m->pagination($pag_size,$offset);
		$this->load->view("head_m",$data);
		$this->load->view('landing_v',$data);
		$this->load->view("footer_m");
		
	}
}
