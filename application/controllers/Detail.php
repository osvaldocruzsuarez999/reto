<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller{
    public function product($id)
	{
        $data["id"] = $id;
        $this->load->model("productos_m");
        $data["datos"] = $this->productos_m->detail($id);
        
        $this->load->view("head_m");
        $this->load->view('detail_m',$data);
        $this->load->view("footer_m");
            
    }
}
