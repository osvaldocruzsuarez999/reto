<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Productos_m extends CI_model{

 
    public function up($data,$return_id = false){
        
        $this->db->insert("productos",$data);
        return $return_id ? $this->db->insert_id() : TRUE;
    }
    public function lista_pro(){
        $cmd = "SELECT * FROM productos";
        $query = $this->db->query($cmd);
        return $query->num_rows() >= 1 ? $query->result() : null;
    }
    public function find(/*$id*/){
        $this->db->select();
        $this->db->from("productos");
        //$this->db->where("id_prod",$id);
        $query = $this->db->get();
        return  $query->row();
    }

    public function findall(){
        $cmd = "SELECT * FROM productos";
        $query = $this->db->query($cmd);
        return $query->num_rows() >= 1 ? $query->result() : null;
    
    }
    public function count(){
        $this->db->select();
        $this->db->from("productos");
        $query = $this->db->get();
        return  $query->num_rows();
    
    }
    public function pagination($pag_size,$offset){
        $this->db->select();
        $this->db->from("productos");
        //$this->db->limit($pag_size,$offset);
    
        $query = $this->db->get();
        return  $query->result();
    
    }
    public function detail($id){
        $cmd = "SELECT * FROM productos WHERE id_prod = ".$id;
        $query = $this->db->query($cmd);
        return $query->num_rows() >= 1 ? $query->result() : null;
       
    }
    public function delete($id){
        $this->db->where('id_prod', $id);
        $this->db->delete('productos');
        return $this->db->affected_rows();
    }
    public function edit($id,$dataarray){
        try {
            $this->db->where("id_prod",$id);
            $this->db->update("productos",$dataarray);
            return true;  
          } catch (Exception $e) {
              return $e->getMessage();
          }

    }
}