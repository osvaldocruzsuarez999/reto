-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-07-2022 a las 11:32:34
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_prod` int(11) NOT NULL,
  `titulo_prod` text DEFAULT NULL,
  `desc_prod` text DEFAULT NULL,
  `img_prod` text DEFAULT NULL,
  `stock_prod` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_prod`, `titulo_prod`, `desc_prod`, `img_prod`, `stock_prod`) VALUES
(2, 'vsagar1', 'Dasypyrum villosum (L.) Coss. & Durieu ex P. Candargy', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 344),
(7, 'lgrise6', 'Arundina Blume', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 222),
(8, 'llocker7', 'Lysiloma divaricatum (Jacq.) J.F. Macbr.', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 19),
(9, 'bbasillon8', 'Fissidens pellucidus Hornsch.', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 82),
(10, 'senbury9', 'Sabatia arenicola Greenm.', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 51),
(11, 'joldcotea', 'Philadelphus mearnsii W.H. Evans ex Koehne', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 16),
(12, 'cortellsb', 'Descurainia pinnata (Walter) Britton ssp. halictorum (Cockerell) Detling', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 9),
(13, 'pbenjafieldc', 'Poa napensis Beetle', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 19),
(14, 'alittleyd', 'Hydrocotyle verticillata Thunb. var. fetherstoniana (Jennings) Mathias', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 74),
(15, 'rhogsdene', 'Chaenotheca gracillima (Vain.) Tibell', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 18),
(16, 'wgaenorf', 'Hymenoxys helenioides (Rydb.) Cockerell', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 10),
(17, 'vdunlopg', 'Viola ×hollickii House', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 18),
(18, 'ajosselsonh', 'Chenopodium polyspermum L. var. acutifolium (Sm.) Gaudich.', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 6),
(19, 'lmontieri', 'Solidago radula Nutt. var. stenolepis Fernald', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 60),
(20, 'ipillingtonj', 'Calochortus elegans Pursh', 'static/upload/9f00a3e2221f0b93ae3ef9fd83bf8d0b.jpg', 60);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre_usuario` text NOT NULL,
  `correo_usuario` text NOT NULL,
  `pass_usuario` text NOT NULL,
  `tipo_user` varchar(255) NOT NULL DEFAULT 'cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre_usuario`, `correo_usuario`, `pass_usuario`, `tipo_user`) VALUES
(5, 'osvaldo', 'osvaldo@gmail.com', 'a45cf22c309004a4e2cc7a47e999e868', 'cliente'),
(6, 'osvaldo', 'osvaldo111@gmail.com', '48685a01fdb37ed0844051ddadcd3557', 'cliente'),
(7, 'asdas', 'osva12@gmail.com', '202cb962ac59075b964b07152d234b70', 'cliente'),
(8, 'asdad', 'sdadas@gmail.com', '98c6c14acce440c6ab3058d2970d5a0f', 'cliente'),
(9, 'osvaldo', 'osfasfas@gmail.com', 'a8f5f167f44f4964e6c998dee827110c', 'cliente'),
(10, 'osvaldo', 'osfasfas@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 'cliente'),
(11, 'admin', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_prod`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
