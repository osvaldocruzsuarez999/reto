$( document ).ready(function() {
//    alert( "ready!" );
});
function registar(){
if(document.getElementById("Inputname").value =="" || document.getElementById("Inputemail").value=="" || document.getElementById("InputPassword").value==""){
    Swal.fire({
        icon: 'error',
        title: 'Llene todos los campos',
        showConfirmButton: false,
        timer: 1500
      })
}else{
    $.ajax({
        url :base_url+"Account/create_service",
        type:"post",
        dataType:"json",
        data : {
            name :document.getElementById("Inputname").value,
            mail: document.getElementById("Inputemail").value,
            pass: document.getElementById("InputPassword").value,

        },
        success : function (json) {
        console.log(json.response_code)
        
        if(json.response_code==200){
            Swal.fire({
                icon: 'success',
                title: 'Guardado',
                showConfirmButton: false,
                timer: 1500
              }).then(function(){
                location.href = base_url+"login"
              })
           }else{
            Swal.fire({
                icon: 'error',
                title: 'Ocurrio un error intenta de nuevo',
                showConfirmButton: false,
                timer: 1500
              })
           }
        }
    })

}
    
}
function login(){
    if(document.getElementById("correo").value ==""|| document.getElementById("password").value ==""){
        Swal.fire({
            icon: 'error',
            title: 'Falta correo o contraseña',
            showConfirmButton: false,
            timer: 1500
          })
    }else{
        $.ajax({
            url:base_url+"Account/auth_service",
            type:"post",
            dataType : "json",
            data : {
                mail:document.getElementById("correo").value,
                pass:document.getElementById("password").value,
            },
            success: function(json){
                if(json.response_code==200){

                    Swal.fire({
                        icon: 'success',
                        title: 'Acceso correcto, Bienvenido',
                        showConfirmButton: false,
                        timer: 1500
                      }).then(function(){
                        location.href = `${base_url}Dashboard/ListProducts`
                      })

                }if(json.response_code==400){
                    Swal.fire({
                        icon: 'error',
                        title: 'Ocurrio un error revisa tus credenciales',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }
        })
    }
}